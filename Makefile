DIR := ${CURDIR}


.PHONY: protoc
protoc:
	./scripts/generate-apartment
	./scripts/generate-device
	./scripts/generate-access
	./scripts/generate-organization
	./scripts/generate-door
	./scripts/generate-deliverybox
	./scripts/generate-authentication

.PHONEY: protoc-lint
protoc-lint:
	docker run --rm -v ${DIR}:/workspace -w /workspace bufbuild/buf check lint
